#include <iostream>

using namespace std;

int main()
{
    int Numero_Matriz_Cuadrada,Posibles_Caminos,factorial_Numerador=1,Factorial_Denomimador1=1,Factorial_Denominador2=1;
                                                                            //Para este programa usamos los conceptos de combinacion para sacar de manera matematica los posibles casos que podemos sacar
    cout<<"Ingrese el numero n que definira su matriz de nxn: \n";
    cin>>Numero_Matriz_Cuadrada;
    for(int i=1;i<= 2*Numero_Matriz_Cuadrada;i++){       //En este ciclo hallaremos el denominador de la formula de combinacion para el calculo de los posibles caminos
        factorial_Numerador= factorial_Numerador*i;
                                                            //Usamos esta combinacion para sacar los posibles casos 2*tamaño de matriz COMBINADO tamaño de matriz: ejm matriz 2x2  2*2COMBINADO 2
    }
    for (int i=1;i<= 2*Numero_Matriz_Cuadrada-Numero_Matriz_Cuadrada;i++){
        Factorial_Denomimador1=Factorial_Denomimador1*i;
    }
    for(int i=1; i<=Numero_Matriz_Cuadrada;i++){
        Factorial_Denominador2=Factorial_Denominador2*i;
    }
    Posibles_Caminos=factorial_Numerador/(Factorial_Denomimador1*Factorial_Denominador2);
    cout<< Posibles_Caminos<<endl;
    return 0;
}
